from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from .forms import PostForm
from .models import Post
 
def home(request):
  postagens = Post.objects.all()
  return render(request,'BLOG/home.html',{'postagens':postagens})
 
@login_required
def postagem(request):
  postagens = Post.objects.all()
  return render(request,'BLOG/postagem.html',{'postagens':postagens})
 
@login_required
def novo_post(request):
  if request.method == 'POST':
     form = PostForm(request.POST)
     if form.is_valid():
        u = form.save()
        u.save()
        return redirect(request.GET.get('next','/postagens/'))
     else:
        messages.error(request, 'Verifique os dados do Post')
  else:
     form = PostForm()
  return render(request, 'BLOG/novo_post.html', {'form': form})
 
@login_required
def editar_post(request,id_post):
  postagem = Post.objects.get(id=id_post)
  if request.method == 'POST':
     form = PostForm(data=request.POST, instance=postagem)
     if form.is_valid():
        u = form.save()
        u.save()
        return redirect(request.GET.get('next','/postagens/'))
     else:
        messages.error(request, 'Verifique os dados do Post')
  else:
     form = PostForm(instance=postagem)
  return render(request, 'BLOG/editar_post.html', {'form': form})
 
@login_required
def deletar_post(request,id_post):
  postagem = Post.objects.get(id=id_post)
  postagem.delete()
  return redirect(request.GET.get('next','/postagens/'))